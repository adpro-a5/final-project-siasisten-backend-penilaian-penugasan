package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository.penilaian;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penilaian.KriteriaPenilaian;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penilaian.PenilaianMahasiswa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PenilaianMahasisiwaRepository extends JpaRepository<PenilaianMahasiswa, String> {
    List<PenilaianMahasiswa> findAllByIdMataKuliahOrderByNpmMahasiswaAscIdNilaiAsc(String idMataKuliah);

    PenilaianMahasiswa findByIdMataKuliahAndNpmMahasiswaAndIdNilai(String idMataKuliah, String npm, KriteriaPenilaian nilai);
}

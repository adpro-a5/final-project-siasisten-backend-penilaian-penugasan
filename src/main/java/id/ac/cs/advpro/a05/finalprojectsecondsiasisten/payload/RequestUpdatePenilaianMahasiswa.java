package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.payload;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penilaian.PenilaianMahasiswa;
import lombok.Data;

import java.util.List;

@Data
public class RequestUpdatePenilaianMahasiswa {
    List<PenilaianMahasiswa> penilaianMahasiswaList;
}

package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.controller.penilaian;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penilaian.PenilaianMahasiswa;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.payload.RequestUpdatePenilaianMahasiswa;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penilaian.PenilaianMahasiswaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"https://siasisten-plus.netlify.app/", "http://localhost:3001/"})
@RestController
@RequestMapping("/api/penilaian-mahasiswa")
public class PenilaianMahasiswaApiController {

    @Autowired
    private PenilaianMahasiswaService penilaianMahasiswaService;

    @GetMapping(produces = {"application/json"})
    public ResponseEntity<List<PenilaianMahasiswa>> getAllKriteria() {
        return ResponseEntity.ok(penilaianMahasiswaService.fetchAllPenilaian());
    }

    @GetMapping(path = "/{id}", produces = {"application/json"})
    public ResponseEntity<List<PenilaianMahasiswa>> getAllKriteriaByIdMataKuliah(@PathVariable(value = "id") String id) {
        return ResponseEntity.ok(penilaianMahasiswaService.listPenilaian(id));
    }

    @PostMapping(value = "", produces = {"application/json"})
    public ResponseEntity<Object> penilaian(@RequestBody RequestUpdatePenilaianMahasiswa updatePenilaianMahasiswa) {
        penilaianMahasiswaService.updateNilai(updatePenilaianMahasiswa);
        return ResponseEntity.ok((HttpStatus.OK));
    }
}

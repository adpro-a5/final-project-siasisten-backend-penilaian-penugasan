package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penugasan;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity.Tugas;

import java.util.List;

public interface TugasService {
    List<Tugas> fetchAllTugas();
    List<Tugas> fetchAllTugasByMataKuliah(String idMataKuliah);
    Tugas fetchAllByIdMataKuliahAndJudulTugas(String idMataKuliah, String judulTugas);
    void createTugas(String idMataKuliah, String judulTugas);
    void deleteTugas(String idTugas);
}

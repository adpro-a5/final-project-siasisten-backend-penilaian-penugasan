package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity;

public enum TugasStatuses {
    UNAVAILABLE, WORKING, FINISHED;
}

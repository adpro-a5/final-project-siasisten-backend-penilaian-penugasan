package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.command;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity.Tugas;

public class SetStatusFinishedCommand implements CommandInterface {
    Tugas tugas;

    public SetStatusFinishedCommand(Tugas tugas) {
        this.tugas = tugas;
    }

    @Override
    public void execute() {
        tugas.setStatusFinished();
    }

    @Override
    public String getName() {
        return String.format("%s:FINISHED", tugas.getIdTugas());
    }

    @Override
    public Tugas getTugas() {
        return tugas;
    }
}

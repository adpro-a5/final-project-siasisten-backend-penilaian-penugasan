package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penilaian;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Table(name = "KriteriaPenilaian")
@NoArgsConstructor
public class KriteriaPenilaian {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false)
    private int id;

    @Column(nullable = false)
    private String idMataKuliah;

    @Column(nullable = false)
    private String namaNilai;

    public KriteriaPenilaian(String idMataKuliah, String namaNilai) {
        this.idMataKuliah = idMataKuliah;
        this.namaNilai = namaNilai;
    }
}

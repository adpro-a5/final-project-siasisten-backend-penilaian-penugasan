package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "TugasLog")
@NoArgsConstructor
public class TugasLog implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_tugas_log")
    private int idTugasLog;

    @Column(nullable = false)
    private String emailAsisten;

    @Column(nullable = false)
    private String isiLog;

    @ManyToOne(cascade=CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "id_tugas")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Tugas tugas;
}

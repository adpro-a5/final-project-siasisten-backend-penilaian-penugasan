package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.controller.penugasan;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.dto.TugasAsistenRelationshipDTO;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity.TugasAsistenRelationship;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penugasan.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/tugas-asisten-relationship")
public class TugasAsistenRelationshipApiController {

    @Autowired
    private TugasAsistenRelationshipService tugasAsistenRelationshipService;

    @GetMapping(produces = {"application/json"})
    public ResponseEntity<List<TugasAsistenRelationship>> getAllTugasAsistenRelationship() {
        return ResponseEntity.ok(tugasAsistenRelationshipService.fetchAllTugasAsistenRelationship());
    }

    @GetMapping(path = "/{id}", produces = {"application/json"})
    public ResponseEntity<TugasAsistenRelationship> getAllByIdTugasAsistenRelationship(@PathVariable(value = "id") int id) {
        return ResponseEntity.ok(tugasAsistenRelationshipService.fetchAllByIdTugasAsistenRelationship(id));
    }

    @GetMapping(path = "/all-relation-by-tugas-id/{idTugas}", produces = {"application/json"})
    public ResponseEntity<List<TugasAsistenRelationship>> getAllByTugasIdTugas(@PathVariable(value = "idTugas") int idTugas) {
        return ResponseEntity.ok(tugasAsistenRelationshipService.fetchAllByTugasIdTugas(idTugas));
    }

    @GetMapping(path = "/all-relation-by-email-asisten/{emailAsisten}", produces = {"application/json"})
    public ResponseEntity<List<TugasAsistenRelationship>> getAllByTugasIdTugas(@PathVariable(value = "emailAsisten") String emailAsisten) {
        return ResponseEntity.ok(tugasAsistenRelationshipService.fetchAllByEmailAsisten(emailAsisten));
    }

    @PostMapping(value = "", produces = {"application/json"})
    public ResponseEntity<Object> postTugasAsistenRelationship(@RequestBody TugasAsistenRelationshipDTO tugasAsistenRelationshipDTO) {
        tugasAsistenRelationshipService.createTugasAsistenRelationship(
                tugasAsistenRelationshipDTO.getIdTugas(),
                tugasAsistenRelationshipDTO.getEmailAsisten()
        );

        return ResponseEntity.ok(HttpStatus.OK);
    }

    @DeleteMapping(path = "/delete", produces = {"application/json"})
    public ResponseEntity<Object> deleteTugas(@RequestBody TugasAsistenRelationshipDTO tugasAsistenRelationshipDTO) {
        tugasAsistenRelationshipService.deleteTugasAsistenRelationship(
                tugasAsistenRelationshipDTO.getIdTugas(),
                tugasAsistenRelationshipDTO.getEmailAsisten()
        );
        return ResponseEntity.ok(HttpStatus.OK);
    }
}

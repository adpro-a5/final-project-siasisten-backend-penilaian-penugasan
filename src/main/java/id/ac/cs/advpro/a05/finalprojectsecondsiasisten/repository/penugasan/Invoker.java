package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository.penugasan;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.command.CommandInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.ElementCollection;
import java.util.HashMap;
import java.util.Map;

@Repository
public class Invoker {
    @Autowired
    TugasRepository tugasRepository;


    @ElementCollection
    private Map<String, CommandInterface> commands;

    public Invoker() {
        this.commands = new HashMap<>();
    }

    public void registerCommand(CommandInterface command) {
        commands.put(command.getName(), command);
    }

    public void execute(String command) {
        CommandInterface temp = commands.get(command);
        temp.execute();
        tugasRepository.save(temp.getTugas());
    }

    public Iterable<String> getCommandNames() {
        return commands.keySet();
    }

    public Map<String, CommandInterface> getCommands() {
        return commands;
    }
}

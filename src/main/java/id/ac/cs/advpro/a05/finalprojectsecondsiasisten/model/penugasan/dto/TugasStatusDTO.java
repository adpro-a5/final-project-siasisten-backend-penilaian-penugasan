package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.dto;

import lombok.Data;

@Data
public class TugasStatusDTO {
    private int idTugas;
    private String status;
}

package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository.penugasan;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity.TugasAsistenRelationship;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TugasAsistenRelationshipRepository extends JpaRepository<TugasAsistenRelationship, String> {
    List<TugasAsistenRelationship> findAllByEmailAsisten(String emailAsisten);
    List<TugasAsistenRelationship> findAllByTugasIdTugas(int idTugas);
    TugasAsistenRelationship findAllByIdTugasAsistenRelationship(int idTugasAsistenRelationship);

    TugasAsistenRelationship findAllByTugasIdTugasAndAndEmailAsisten(int idTugas, String emailAsisten);
}

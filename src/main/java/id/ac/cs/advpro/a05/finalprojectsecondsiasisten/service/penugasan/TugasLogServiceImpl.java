package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penugasan;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity.Tugas;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity.TugasLog;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository.penugasan.TugasLogRepository;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository.penugasan.TugasRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TugasLogServiceImpl implements TugasLogService{
    @Autowired
    TugasLogRepository tugasLogRepository;

    @Autowired
    TugasRepository tugasRepository;


    @Override
    public List<TugasLog> fetchAllTugasLog() {
        return tugasLogRepository.findAll();
    }

    @Override
    public List<TugasLog> fetchAllByTugasIdTugas(int tugasId) {
        return tugasLogRepository.findAllByTugasIdTugas(tugasId);
    }

    @Override
    public List<TugasLog> fetchAllByIdTugasLog(int idTugasLog) {
        return tugasLogRepository.findAllByIdTugasLog(idTugasLog);
    }

    @Override
    public void createTugasLog(int tugasId, String emailAsisten, String isiLog) {
        TugasLog tugasLog = new TugasLog();
        tugasLog.setEmailAsisten(emailAsisten);
        tugasLog.setIsiLog(isiLog);

        Tugas targetTugas = tugasRepository.findAllByIdTugas(tugasId);
        tugasLog.setTugas(targetTugas);
        tugasLogRepository.save(tugasLog);
    }

}

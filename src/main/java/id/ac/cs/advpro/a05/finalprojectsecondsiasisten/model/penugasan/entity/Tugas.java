package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "Tugas")
@NoArgsConstructor
public class Tugas implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_tugas")
    private int idTugas;

    @Column(nullable = false)
    private String idMataKuliah;

    @Column(nullable = false)
    private String judulTugas;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private TugasStatuses status;

    public void setStatusWorking() {
        this.status = TugasStatuses.WORKING;
    }

    public void setStatusUnavailable() {
        this.status = TugasStatuses.UNAVAILABLE;
    }

    public void setStatusFinished() {
        this.status = TugasStatuses.FINISHED;
    }
}

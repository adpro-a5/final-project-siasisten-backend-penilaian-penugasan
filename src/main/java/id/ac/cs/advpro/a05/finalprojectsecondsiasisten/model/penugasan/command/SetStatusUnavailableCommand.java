package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.command;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity.Tugas;

public class SetStatusUnavailableCommand implements CommandInterface {
    Tugas tugas;

    public SetStatusUnavailableCommand(Tugas tugas) {
        this.tugas = tugas;
    }

    @Override
    public void execute() {
        tugas.setStatusUnavailable();
    }

    @Override
    public String getName() {
        return String.format("%s:UNAVAILABLE", tugas.getIdTugas());
    }

    @Override
    public Tugas getTugas() {
        return tugas;
    }
}

package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "TugasAsistenRelationship")
@NoArgsConstructor
public class TugasAsistenRelationship implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int idTugasAsistenRelationship;

    @Column(nullable = false)
    private String emailAsisten;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_tugas")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Tugas tugas;
}

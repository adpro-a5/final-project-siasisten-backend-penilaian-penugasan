package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.controller.penilaian;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penilaian.MahasiswaMatkul;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.payload.RequestCreateMahasiswaMatkul;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penilaian.MahasiswaMatkulService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"https://siasisten-plus.netlify.app/", "http://localhost:3001/"})
@RestController
@RequestMapping("/api/mahasiswa-matkul")
public class MahasiswaMatkulApiController {

    @Autowired
    private MahasiswaMatkulService mahasiswaMatkulService;

    @GetMapping(produces = {"application/json"})
    public ResponseEntity<List<MahasiswaMatkul>> getAllMahasiswaMatkul() {
        return ResponseEntity.ok(mahasiswaMatkulService.fetchAllMahasiswaMatkul());
    }

    @GetMapping(path = "/{id}", produces = {"application/json"})
    public ResponseEntity<List<MahasiswaMatkul>> getAllKriteriaByIdMataKuliah(@PathVariable(value = "id") String id) {
        return ResponseEntity.ok(mahasiswaMatkulService.listMahasiswa(id));
    }

    @PostMapping(produces = {"application/json"})
    public ResponseEntity<Object> createMahasiswaMatkul(@RequestBody RequestCreateMahasiswaMatkul mahasiswaMatkul) {
        mahasiswaMatkulService.addMahasiswaMataKuliah(
                mahasiswaMatkul.getListMahasiswaMatkul(),
                mahasiswaMatkul.getIdMataKuliah());
        return ResponseEntity.ok(HttpStatus.OK);
    }
}

package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository.penilaian;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penilaian.MahasiswaMatkul;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MahasiswaMatkulRepository extends JpaRepository<MahasiswaMatkul, String> {
    List<MahasiswaMatkul> findAllByIdMataKuliahOrderByNpmMahasiswaAsc(String idMataKuliah);

    List<MahasiswaMatkul> findAllByIdMataKuliah(String idMataKuliah);

    MahasiswaMatkul findMahasiswaMatkulByIdMataKuliahAndNpmMahasiswa(String idMataKuliah, String npm);
}

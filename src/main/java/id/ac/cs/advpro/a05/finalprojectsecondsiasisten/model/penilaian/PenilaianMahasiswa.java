package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penilaian;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Table(name = "PenilaianMahasisiwa")
@NoArgsConstructor
public class PenilaianMahasiswa {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false)
    private int id;

    @Column(nullable = false)
    private String npmMahasiswa;

    @Column(nullable = false)
    private String idMataKuliah;

    @ManyToOne
    @JoinColumn(name = "id_nilai", nullable = false)
    private KriteriaPenilaian idNilai;

    @Column
    private int nilai;

    public PenilaianMahasiswa(String idMataKuliah, String npmMahasiswa, KriteriaPenilaian idNilai) {
        this.idMataKuliah = idMataKuliah;
        this.npmMahasiswa = npmMahasiswa;
        this.idNilai = idNilai;
    }
}

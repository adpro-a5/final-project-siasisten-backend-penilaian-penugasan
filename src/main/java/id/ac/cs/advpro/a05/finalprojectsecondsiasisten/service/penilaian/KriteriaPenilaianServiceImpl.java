package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penilaian;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penilaian.KriteriaPenilaian;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penilaian.MahasiswaMatkul;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penilaian.PenilaianMahasiswa;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository.penilaian.KriteriaPenilaianRepository;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository.penilaian.MahasiswaMatkulRepository;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository.penilaian.PenilaianMahasisiwaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class KriteriaPenilaianServiceImpl implements KriteriaPenilaianService {

    @Autowired
    KriteriaPenilaianRepository kriteriaPenilaianRepository;

    @Autowired
    PenilaianMahasisiwaRepository penilaianMahasisiwaRepository;

    @Autowired
    MahasiswaMatkulRepository mahasiswaMatkulRepository;

    @Override
    public List<KriteriaPenilaian> fetchAllKriteriaPenilaian() {
        return kriteriaPenilaianRepository.findAll();
    }

    @Override
    public List<KriteriaPenilaian> fetchAllKriteriaPenilaianByMataKuliah(String idMataKuliah) {
        return kriteriaPenilaianRepository.findAllByIdMataKuliahOrderByNamaNilaiAsc(idMataKuliah);
    }

    @Override
    public void addKriteriaPenilaian(List<String> kriteriaList, String idMataKuliah) {
        Set<String> mahasiswaList = parseMahasiswaMatkul(
                mahasiswaMatkulRepository.findAllByIdMataKuliah(idMataKuliah));

        for (String kriteria : kriteriaList) {
            if (kriteriaPenilaianRepository.findKriteriaPenilaianByNamaNilaiAndIdMataKuliah
                    (kriteria, idMataKuliah) == null) {
                KriteriaPenilaian kriteriaPenilaian = new KriteriaPenilaian(idMataKuliah, kriteria);
                kriteriaPenilaianRepository.save(kriteriaPenilaian);
                for (String mahasiswa : mahasiswaList) {
                    penilaianMahasisiwaRepository.save(
                            new PenilaianMahasiswa(idMataKuliah, mahasiswa, kriteriaPenilaian));
                }
            }
        }
    }

    public Set<String> parseMahasiswaMatkul(List<MahasiswaMatkul> mahasiswaMatkuls) {
        Set<String> listNPM = new HashSet<>();

        for (MahasiswaMatkul mahasiswaMatkul: mahasiswaMatkuls) {
            listNPM.add(mahasiswaMatkul.getNpmMahasiswa());
        }

        return listNPM;
    }
}

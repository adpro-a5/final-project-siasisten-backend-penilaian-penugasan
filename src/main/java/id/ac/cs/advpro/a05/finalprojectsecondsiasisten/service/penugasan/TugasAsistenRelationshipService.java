package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penugasan;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity.TugasAsistenRelationship;

import java.util.List;

public interface TugasAsistenRelationshipService {
    TugasAsistenRelationship fetchAllByIdTugasAsistenRelationship(int idTugasAsistenRelationship);
    List<TugasAsistenRelationship> fetchAllTugasAsistenRelationship();
    List<TugasAsistenRelationship> fetchAllByTugasIdTugas(int tugasId);
    List<TugasAsistenRelationship> fetchAllByEmailAsisten(String emailAsisten);
    void createTugasAsistenRelationship(int idTugas, String emailAsisten);
    void deleteTugasAsistenRelationship(int idTugas, String emailAsisten);
}

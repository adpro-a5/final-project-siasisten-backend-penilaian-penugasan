package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penilaian;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penilaian.MahasiswaMatkul;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository.penilaian.MahasiswaMatkulRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MahasiswaMatkulServiceImpl implements MahasiswaMatkulService{

    @Autowired
    private MahasiswaMatkulRepository mahasiswaMatkulRepository;

    @Autowired
    private PenilaianMahasiswaService penilaianMahasiswaService;

    @Override
    public List<MahasiswaMatkul> listMahasiswa(String idMataKuliah) {
        return mahasiswaMatkulRepository.findAllByIdMataKuliahOrderByNpmMahasiswaAsc(idMataKuliah);
    }

    @Override
    public List<MahasiswaMatkul> fetchAllMahasiswaMatkul() {
        return mahasiswaMatkulRepository.findAll();
    }

    @Override
    public void addMahasiswaMataKuliah(List<String> mahasiswaList, String idMataKuliah) {
        for (String mahasiswa : mahasiswaList) {
            if (mahasiswaMatkulRepository.findMahasiswaMatkulByIdMataKuliahAndNpmMahasiswa(
                    idMataKuliah, mahasiswa) == null) {
                mahasiswaMatkulRepository.save(new MahasiswaMatkul(idMataKuliah, mahasiswa));
            }
        }
        penilaianMahasiswaService.addPenilaian(mahasiswaList, idMataKuliah);
    }
}

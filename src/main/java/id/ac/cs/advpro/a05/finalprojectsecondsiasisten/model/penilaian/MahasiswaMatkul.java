package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penilaian;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "MahasiswaMatkul")
@NoArgsConstructor
public class MahasiswaMatkul implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false)
    private int id;

    @Column(nullable = false)
    private String idMataKuliah;

    @Column(nullable = false)
    private String npmMahasiswa;

    public MahasiswaMatkul(String idMataKuliah, String npmMahasiswa) {
        this.idMataKuliah = idMataKuliah;
        this.npmMahasiswa = npmMahasiswa;
    }
}

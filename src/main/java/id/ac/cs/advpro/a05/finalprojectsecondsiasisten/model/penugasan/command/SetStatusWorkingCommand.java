package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.command;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity.Tugas;

public class SetStatusWorkingCommand implements CommandInterface {
    Tugas tugas;

    public SetStatusWorkingCommand(Tugas tugas) {
        this.tugas = tugas;
    }

    @Override
    public void execute() {
        tugas.setStatusWorking();
    }

    @Override
    public String getName() {
        return String.format("%s:WORKING", tugas.getIdTugas());
    }

    @Override
    public Tugas getTugas() {
        return tugas;
    }
}

package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penugasan;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity.TugasLog;

import java.util.List;

public interface TugasLogService {
    List<TugasLog> fetchAllTugasLog();
    List<TugasLog> fetchAllByTugasIdTugas(int tugasId);
    List<TugasLog> fetchAllByIdTugasLog(int idTugasLog);
    void createTugasLog(int tugasId, String emailAsisten, String isiLog);
}

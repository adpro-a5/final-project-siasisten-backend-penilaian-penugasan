package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penugasan;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity.Tugas;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity.TugasAsistenRelationship;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository.penugasan.TugasAsistenRelationshipRepository;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository.penugasan.TugasRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TugasAsistenRelationshipServiceImpl implements TugasAsistenRelationshipService{
    @Autowired
    TugasAsistenRelationshipRepository tugasAsistenRelationshipRepository;

    @Autowired
    TugasRepository tugasRepository;

    @Override
    public TugasAsistenRelationship fetchAllByIdTugasAsistenRelationship(int idTugasAsistenRelationship) {
        return tugasAsistenRelationshipRepository.findAllByIdTugasAsistenRelationship(idTugasAsistenRelationship);
    }

    @Override
    public List<TugasAsistenRelationship> fetchAllTugasAsistenRelationship() {
        return tugasAsistenRelationshipRepository.findAll();
    }

    @Override
    public List<TugasAsistenRelationship> fetchAllByTugasIdTugas(int tugasId) {
        return tugasAsistenRelationshipRepository.findAllByTugasIdTugas(tugasId);
    }

    @Override
    public List<TugasAsistenRelationship> fetchAllByEmailAsisten(String emailAsisten) {
        return tugasAsistenRelationshipRepository.findAllByEmailAsisten(emailAsisten);
    }

    @Override
    public void createTugasAsistenRelationship(int idTugas, String emailAsisten) {
        TugasAsistenRelationship tugasAsistenRelationship = new TugasAsistenRelationship();
        tugasAsistenRelationship.setEmailAsisten(emailAsisten);

        Tugas targetTugas = tugasRepository.findAllByIdTugas(idTugas);
        tugasAsistenRelationship.setTugas(targetTugas);
        tugasAsistenRelationshipRepository.save(tugasAsistenRelationship);
    }

    @Override
    public void deleteTugasAsistenRelationship(int idTugas, String emailAsisten) {
        TugasAsistenRelationship tugasAsistenRelationship =
                tugasAsistenRelationshipRepository.
                        findAllByTugasIdTugasAndAndEmailAsisten(idTugas, emailAsisten);
        tugasAsistenRelationshipRepository.delete(tugasAsistenRelationship);
    }

}

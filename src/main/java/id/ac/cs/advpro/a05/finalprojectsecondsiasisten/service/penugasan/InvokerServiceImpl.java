package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penugasan;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.command.CommandInterface;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.command.SetStatusFinishedCommand;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.command.SetStatusUnavailableCommand;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.command.SetStatusWorkingCommand;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity.Tugas;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository.penugasan.Invoker;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository.penugasan.TugasRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class InvokerServiceImpl implements InvokerService{
    @Autowired
    private TugasRepository tugasRepository;

    @Autowired
    private Invoker invoker;

    @Override
    public void executeCommand(String command) {
        Map<String, CommandInterface>  commands = invoker.getCommands();
        CommandInterface temp = commands.get(command);

        if (temp != null) {
            invoker.execute(command);
        }

        String[] deconstructedCommand = command.split(":");
        String tugasId = deconstructedCommand[0];
        Tugas tempTugas = tugasRepository.findAllByIdTugas(Integer.parseInt(tugasId));

        if (tempTugas != null) {
            createTugasCommand(tempTugas);
            invoker.execute(command);
        }
    }

    @Override
    public void setStatus(int idTugas, String status) {
        String commandToExecute = idTugas + ":" + status;
        executeCommand(commandToExecute);
    }

    @Override
    public void createTugasCommand(Tugas tugas) {
        invoker.registerCommand(new SetStatusUnavailableCommand(tugas));
        invoker.registerCommand(new SetStatusWorkingCommand(tugas));
        invoker.registerCommand(new SetStatusFinishedCommand(tugas));
    }
}

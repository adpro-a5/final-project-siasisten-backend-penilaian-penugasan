package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penugasan;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity.Tugas;

public interface InvokerService {
    void executeCommand(String command);
    void setStatus(int idTugas, String status);
    void createTugasCommand(Tugas tugas);
}

package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.payload;

import lombok.Data;

import java.util.List;

@Data
public class RequestCreateKriteriaPenilaian {
    String idMataKuliah;
    List<String> listKriteriaPenilaian;
}

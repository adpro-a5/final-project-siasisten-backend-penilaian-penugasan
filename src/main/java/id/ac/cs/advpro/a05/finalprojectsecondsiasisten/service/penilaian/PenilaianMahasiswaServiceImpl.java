package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penilaian;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penilaian.KriteriaPenilaian;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penilaian.PenilaianMahasiswa;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.payload.RequestUpdatePenilaianMahasiswa;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository.penilaian.KriteriaPenilaianRepository;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository.penilaian.MahasiswaMatkulRepository;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository.penilaian.PenilaianMahasisiwaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PenilaianMahasiswaServiceImpl implements PenilaianMahasiswaService {

    @Autowired
    PenilaianMahasisiwaRepository penilaianMahasisiwaRepository;

    @Autowired
    KriteriaPenilaianRepository kriteriaPenilaianRepository;

    @Autowired
    MahasiswaMatkulRepository mahasiswaMatkulRepository;

    @Override
    public List<PenilaianMahasiswa> fetchAllPenilaian() {
        return penilaianMahasisiwaRepository.findAll();
    }

    @Override
    public List<PenilaianMahasiswa> listPenilaian(String idMataKuliah) {
        return penilaianMahasisiwaRepository.findAllByIdMataKuliahOrderByNpmMahasiswaAscIdNilaiAsc(idMataKuliah);
    }

    @Override
    public void addPenilaian(List<String> mahasiswaList, String idMataKuliah) {
        for (KriteriaPenilaian kriteriaPenilaian : kriteriaPenilaianRepository.findAllByIdMataKuliah(idMataKuliah)) {
            for (String mahasiswa : mahasiswaList) {
                if (penilaianMahasisiwaRepository.findByIdMataKuliahAndNpmMahasiswaAndIdNilai(
                        idMataKuliah, mahasiswa, kriteriaPenilaian) == null) {
                    penilaianMahasisiwaRepository.save(new PenilaianMahasiswa(idMataKuliah, mahasiswa, kriteriaPenilaian));
                }
            }
        }
    }

    @Override
    public void updateNilai(RequestUpdatePenilaianMahasiswa updatePenilaianMahasiswa) {
        penilaianMahasisiwaRepository.saveAll(updatePenilaianMahasiswa.getPenilaianMahasiswaList());
    }
}

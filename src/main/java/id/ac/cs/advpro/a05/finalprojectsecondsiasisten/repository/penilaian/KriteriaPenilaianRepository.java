package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository.penilaian;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penilaian.KriteriaPenilaian;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KriteriaPenilaianRepository extends JpaRepository<KriteriaPenilaian, String> {
    List<KriteriaPenilaian> findAllByIdMataKuliahOrderByNamaNilaiAsc(String idMataKuliah);

    List<KriteriaPenilaian> findAllByIdMataKuliah(String idMataKuliah);

    KriteriaPenilaian findKriteriaPenilaianById(int id);

    KriteriaPenilaian findKriteriaPenilaianByNamaNilaiAndIdMataKuliah(String namaNilai, String idMataKuliah);
}

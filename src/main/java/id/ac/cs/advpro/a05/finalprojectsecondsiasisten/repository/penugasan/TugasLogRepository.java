package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository.penugasan;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity.TugasLog;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TugasLogRepository extends JpaRepository<TugasLog, String> {
    List<TugasLog> findAllByIdTugasLog(int idTugasLog);

    List<TugasLog> findAllByTugasIdTugas(int idTugas);
}

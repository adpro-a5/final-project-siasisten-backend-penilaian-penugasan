package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penilaian;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penilaian.PenilaianMahasiswa;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.payload.RequestUpdatePenilaianMahasiswa;

import java.util.List;

public interface PenilaianMahasiswaService {
    List<PenilaianMahasiswa> fetchAllPenilaian();
    List<PenilaianMahasiswa> listPenilaian(String idMataKuliah);
    void addPenilaian(List<String> mahasiswaList, String idMataKuliah);
    void updateNilai(RequestUpdatePenilaianMahasiswa updatePenilaianMahasiswa);
}

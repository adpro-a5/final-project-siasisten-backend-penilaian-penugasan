package id.ac.cs.advpro.a05.finalprojectsecondsiasisten;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinalProjectSecondSiasistenApplication {

    public static void main(String[] args) {
        SpringApplication.run(FinalProjectSecondSiasistenApplication.class, args);
    }

}

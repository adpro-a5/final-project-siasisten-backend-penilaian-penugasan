package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penilaian;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penilaian.KriteriaPenilaian;

import java.util.List;

public interface KriteriaPenilaianService {
    List<KriteriaPenilaian> fetchAllKriteriaPenilaian();
    List<KriteriaPenilaian> fetchAllKriteriaPenilaianByMataKuliah(String mataKuliah);
    void addKriteriaPenilaian(List<String> kriteriaPenilaianList, String mataKuliah);
}

package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penilaian;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penilaian.MahasiswaMatkul;

import java.util.List;

public interface MahasiswaMatkulService {
    List<MahasiswaMatkul> fetchAllMahasiswaMatkul();
    void addMahasiswaMataKuliah(List<String> mahasiswaList, String idMataKuliah);
    List<MahasiswaMatkul> listMahasiswa(String idMataKuliah);
}

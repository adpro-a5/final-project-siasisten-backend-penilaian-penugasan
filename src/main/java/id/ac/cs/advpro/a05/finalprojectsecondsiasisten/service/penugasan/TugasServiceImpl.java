package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penugasan;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity.Tugas;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository.penugasan.TugasRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TugasServiceImpl implements TugasService{
    @Autowired
    TugasRepository tugasRepository;

    @Override
    public List<Tugas> fetchAllTugas() {
        return tugasRepository.findAll();
    }

    @Override
    public List<Tugas> fetchAllTugasByMataKuliah(String idMataKuliah) {
        return tugasRepository.findAllByIdMataKuliah(idMataKuliah);
    }

    @Override
    public Tugas fetchAllByIdMataKuliahAndJudulTugas(String idMataKuliah, String judulTugas) {
        return tugasRepository.findAllByIdMataKuliahAndJudulTugas(idMataKuliah, judulTugas);
    }

    @Override
    public void createTugas(String idMataKuliah, String judulTugas) {
        Tugas tugas = new Tugas();
        tugas.setIdMataKuliah(idMataKuliah);
        tugas.setJudulTugas(judulTugas);
        tugas.setStatusUnavailable();

        tugasRepository.save(tugas);
    }

    @Override
    public void deleteTugas(String idTugas) {
        Tugas tempTugas = tugasRepository.findAllByIdTugas(Integer.parseInt(idTugas));
        tugasRepository.delete(tempTugas);
    }
}

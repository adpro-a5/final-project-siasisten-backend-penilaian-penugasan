package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.dto;

import lombok.Data;

@Data
public class TugasDTO {
    private String idMataKuliah;
    private String judulTugas;
    private String deskripsiTugas;
}

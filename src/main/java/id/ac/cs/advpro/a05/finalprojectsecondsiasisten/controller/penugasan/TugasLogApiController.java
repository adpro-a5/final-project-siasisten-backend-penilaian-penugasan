package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.controller.penugasan;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.dto.TugasLogDTO;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity.TugasLog;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penugasan.TugasLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/tugas-log")
public class TugasLogApiController {

    @Autowired
    private TugasLogService tugasLogService;

    @GetMapping(produces = {"application/json"})
    public ResponseEntity<List<TugasLog>> getAllTugasLog() {
        return ResponseEntity.ok(tugasLogService.fetchAllTugasLog());
    }

    @GetMapping(path = "/{id}", produces = {"application/json"})
    public ResponseEntity<List<TugasLog>> getAllByIdTugasLog(@PathVariable(value = "id") int id) {
        return ResponseEntity.ok(tugasLogService.fetchAllByIdTugasLog(id));
    }

    @GetMapping(path = "/all-log-by-tugas-id/{idTugas}", produces = {"application/json"})
    public ResponseEntity<List<TugasLog>> getAllByTugasIdTugas(@PathVariable(value = "idTugas") int idTugas) {
        return ResponseEntity.ok(tugasLogService.fetchAllByTugasIdTugas(idTugas));
    }

    @PostMapping(value = "", produces = {"application/json"})
    public ResponseEntity<Object> postTugasLog(@RequestBody TugasLogDTO tugasLogDTO) {
        tugasLogService.createTugasLog(
                tugasLogDTO.getIdTugas(),
                tugasLogDTO.getEmailAsisten(),
                tugasLogDTO.getIsiLog()
        );

        return ResponseEntity.ok(HttpStatus.OK);
    }
}

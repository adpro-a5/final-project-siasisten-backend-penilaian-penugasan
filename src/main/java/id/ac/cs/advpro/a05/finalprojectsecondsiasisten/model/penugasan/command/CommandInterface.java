package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.command;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity.Tugas;

public interface CommandInterface {
    public void execute();
    public String getName();

    public Tugas getTugas();
}

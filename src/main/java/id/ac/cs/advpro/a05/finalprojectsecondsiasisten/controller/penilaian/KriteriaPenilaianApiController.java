package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.controller.penilaian;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penilaian.KriteriaPenilaian;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.payload.RequestCreateKriteriaPenilaian;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penilaian.KriteriaPenilaianService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"https://siasisten-plus.netlify.app/", "http://localhost:3001/"})
@RestController
@RequestMapping("/api/kriteria-penilaian")
public class KriteriaPenilaianApiController {

    @Autowired
    private KriteriaPenilaianService kriteriaPenilaianService;

    @GetMapping(produces = {"application/json"})
    public ResponseEntity<List<KriteriaPenilaian>> getAllKriteria() {
        return ResponseEntity.ok(kriteriaPenilaianService.fetchAllKriteriaPenilaian());
    }

    @GetMapping(path = "/{id}", produces = {"application/json"})
    public ResponseEntity<List<KriteriaPenilaian>> getAllKriteriaByIdMataKuliah(@PathVariable(value = "id") String id) {
        return ResponseEntity.ok(kriteriaPenilaianService.fetchAllKriteriaPenilaianByMataKuliah(id));
    }

    @PostMapping(produces = {"application/json"})
    public ResponseEntity<Object> createKriteriaPenilaian(@RequestBody RequestCreateKriteriaPenilaian kriteriaPenilaian) {
        kriteriaPenilaianService.addKriteriaPenilaian(
                kriteriaPenilaian.getListKriteriaPenilaian(),
                kriteriaPenilaian.getIdMataKuliah());
        return ResponseEntity.ok(HttpStatus.OK);
    }
}

package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.controller.penugasan;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.dto.TugasDTO;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.dto.TugasStatusDTO;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity.Tugas;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penugasan.InvokerService;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penugasan.TugasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/tugas")
public class TugasApiController {

    @Autowired
    private InvokerService invokerService;

    @Autowired
    private TugasService tugasService;

    @GetMapping(produces = {"application/json"})
    public ResponseEntity<List<Tugas>> getAllTugas() {
        return ResponseEntity.ok(tugasService.fetchAllTugas());
    }

    @GetMapping(path = "/{id}", produces = {"application/json"})
    public ResponseEntity<List<Tugas>> getAllTugasByIdMataKuliah(@PathVariable(value = "id") String id) {
        return ResponseEntity.ok(tugasService.fetchAllTugasByMataKuliah(id));
    }

    @PostMapping(value = "", produces = {"application/json"})
    public ResponseEntity<Object> postTugas(@RequestBody TugasDTO tugasDTO) {
        tugasService.createTugas(
                tugasDTO.getIdMataKuliah(),
                tugasDTO.getJudulTugas()
        );
        Tugas createdTugas = tugasService.fetchAllByIdMataKuliahAndJudulTugas(
                tugasDTO.getIdMataKuliah(),
                tugasDTO.getJudulTugas()
        );
        invokerService.createTugasCommand(createdTugas);

        return ResponseEntity.ok(HttpStatus.OK);
    }

    @DeleteMapping(path = "/delete/{id}", produces = {"application/json"})
    public ResponseEntity<Object> deleteTugas(@PathVariable(value = "id") String id) {
        tugasService.deleteTugas(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @PostMapping(value = "/post-status", produces = {"application/json"})
    public ResponseEntity<Object> postTugasStatus(@RequestBody TugasStatusDTO tugasStatusDTO) {
        invokerService.setStatus(
                tugasStatusDTO.getIdTugas(),
                tugasStatusDTO.getStatus()
        );
        return ResponseEntity.ok(HttpStatus.OK);
    }


}

package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository.penugasan;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity.Tugas;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TugasRepository extends JpaRepository<Tugas, String> {
    List<Tugas> findAllByIdMataKuliah(String idMatakuliah);

    Tugas findAllByIdMataKuliahAndJudulTugas(String idMataKuliah, String judulTugas);

    Tugas findAllByIdTugas(int idTugas);
}

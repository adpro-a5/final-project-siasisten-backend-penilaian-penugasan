package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.services.penugasan;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity.Tugas;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity.TugasStatuses;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository.penugasan.TugasRepository;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penugasan.TugasServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TugasServiceTest {
    Class<?> tugasServiceClass;

    @Mock
    TugasRepository tugasRepository;

    @InjectMocks
    private TugasServiceImpl tugasService;

    private List<Tugas> tugasList;

    @BeforeEach
    void setup() throws Exception {
        tugasServiceClass = Class.forName(
                "id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penugasan.TugasServiceImpl");
        Tugas tugas1 = new Tugas();
        tugas1.setIdTugas(1);
        tugas1.setIdMataKuliah("id1");
        tugas1.setJudulTugas("judul1");
        tugas1.setStatus(TugasStatuses.UNAVAILABLE);

        Tugas tugas2 = new Tugas();
        tugas2.setIdTugas(2);
        tugas2.setIdMataKuliah("id2");
        tugas2.setJudulTugas("judul2");
        tugas2.setStatus(TugasStatuses.WORKING);

        Tugas tugas3 = new Tugas();
        tugas3.setIdTugas(3);
        tugas3.setIdMataKuliah("3");
        tugas3.setJudulTugas("judul3");
        tugas3.setStatus(TugasStatuses.FINISHED);

        tugasList = new ArrayList<>();
        tugasList.add(tugas1);
        tugasList.add(tugas2);
        tugasList.add(tugas3);
    }

    @Test
    void testFetchAllTugas() {
        when(tugasRepository.findAll()).thenReturn(tugasList);
        assertEquals(tugasService.fetchAllTugas(), tugasList);
    }

    @Test
    void testFetchAllTugasByMataKuliah() {
        when(tugasRepository.findAllByIdMataKuliah("id1")).thenReturn(tugasList);
        assertEquals(tugasService.fetchAllTugasByMataKuliah("id1"), tugasList);
    }

    @Test
    void testFetchAllByIdMataKuliahAndJudulTugas() {
        when(tugasRepository.findAllByIdMataKuliahAndJudulTugas("id1", "judul1")).thenReturn(tugasList.get(0));
        assertEquals(tugasService.fetchAllByIdMataKuliahAndJudulTugas("id1", "judul1"), tugasList.get(0));
    }

    @Test
    void testCreateDeleteTugas() {
        when(tugasRepository.findAllByIdTugas(0)).thenReturn(tugasList.get(0));
        tugasService.createTugas("id10", "judul10");
        tugasService.deleteTugas("0");
        verify(tugasRepository, times(1)).delete(tugasList.get(0));
    }
}
package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.controller.penilaian;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.controller.Mapper;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.controller.penilaian.PenilaianMahasiswaApiController;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penilaian.KriteriaPenilaian;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penilaian.PenilaianMahasiswa;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.payload.RequestUpdatePenilaianMahasiswa;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penilaian.PenilaianMahasiswaService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = PenilaianMahasiswaApiController.class)
class PenilaianMahasiswaApiControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PenilaianMahasiswaService penilaianMahasiswaService;

    private List<PenilaianMahasiswa> penilaianList;
    private KriteriaPenilaian kriteriaPenilaianLab;
    private RequestUpdatePenilaianMahasiswa mapPenilaian;

    @BeforeEach
    void setUp() {
        kriteriaPenilaianLab = new KriteriaPenilaian("id", "Lab 1");
        PenilaianMahasiswa penilaianMahasiswa1 = new PenilaianMahasiswa("id", "1", kriteriaPenilaianLab);
        PenilaianMahasiswa penilaianMahasiswa2 = new PenilaianMahasiswa("id", "2", kriteriaPenilaianLab);
        penilaianList = new ArrayList<>();
        penilaianList.add(penilaianMahasiswa1);
        penilaianList.add(penilaianMahasiswa2);

        mapPenilaian = new RequestUpdatePenilaianMahasiswa();
        mapPenilaian.setPenilaianMahasiswaList(penilaianList);
    }

    @Test
    void testGetPenilaianMahasiswa() throws Exception {
        when(penilaianMahasiswaService.fetchAllPenilaian()).thenReturn(penilaianList);

        mockMvc.perform(get("/api/penilaian-mahasiswa").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].idMataKuliah").value("id"))
                .andExpect(jsonPath("$[0].npmMahasiswa").value("1"))
                .andExpect(jsonPath("$[0].idNilai").value(kriteriaPenilaianLab))
                .andExpect(jsonPath("$[1].npmMahasiswa").value("2"))
                .andExpect(jsonPath("$[1].idNilai").value(kriteriaPenilaianLab));
    }

    @Test
    void testGetPenilaianMahasiswaById() throws Exception {
        when(penilaianMahasiswaService.listPenilaian("id")).thenReturn(penilaianList);

        mockMvc.perform(get("/api/penilaian-mahasiswa/id").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].idMataKuliah").value("id"))
                .andExpect(jsonPath("$[0].npmMahasiswa").value("1"))
                .andExpect(jsonPath("$[0].idNilai").value(kriteriaPenilaianLab))
                .andExpect(jsonPath("$[1].npmMahasiswa").value("2"))
                .andExpect(jsonPath("$[1].idNilai").value(kriteriaPenilaianLab));
    }

    @Test
    void testPostPenilaianMahasiswa() throws Exception {
        mockMvc.perform(post("/api/penilaian-mahasiswa")
                        .contentType(MediaType.APPLICATION_JSON).content(Mapper.mapToJson(mapPenilaian)))
                .andExpect(status().isOk());

        verify(penilaianMahasiswaService, times(1)).updateNilai(mapPenilaian);
    }
}

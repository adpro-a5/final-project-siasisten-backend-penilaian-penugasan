package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.services.penugasan;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.command.CommandInterface;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.command.SetStatusFinishedCommand;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.command.SetStatusUnavailableCommand;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.command.SetStatusWorkingCommand;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity.Tugas;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository.penugasan.Invoker;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository.penugasan.TugasRepository;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penugasan.InvokerServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class InvokerServiceTest {
    @Mock
    private TugasRepository tugasRepository;

    @Mock
    private Invoker invoker;

    @InjectMocks
    private InvokerServiceImpl invokerService;

    private Tugas tugas;
    private CommandInterface commandInterface1;

    @Mock
    private Map<String, CommandInterface> map;

    @BeforeEach
    void setUp() {
        tugas = new Tugas();
        commandInterface1 = new SetStatusUnavailableCommand(tugas);
        CommandInterface commandInterface2 = new SetStatusWorkingCommand(tugas);
        CommandInterface commandInterface3 = new SetStatusFinishedCommand(tugas);
        map = new HashMap<>();
        map.put(commandInterface1.getName(), commandInterface1);
        map.put(commandInterface2.getName(), commandInterface2);
        map.put(commandInterface3.getName(), commandInterface3);
    }

    @Test
    void testCreateSetAndExecuteCommand() {
        invokerService.setStatus(0, "FINISHED");
        invokerService.createTugasCommand(tugas);
        when(invoker.getCommands()).thenReturn(map);
        invokerService.executeCommand("1:Command");
        verify(invoker, times(0)).execute("1:Command");
    }

    @Test
    void testExecuteCommand() {
        when(invoker.getCommands()).thenReturn(map);
        when(tugasRepository.findAllByIdTugas(0)).thenReturn(new Tugas());
        invokerService.executeCommand(commandInterface1.getName());
        verify(invoker, times(2)).execute(commandInterface1.getName());
    }
}
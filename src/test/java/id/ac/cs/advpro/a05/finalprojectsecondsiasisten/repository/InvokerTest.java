package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.command.CommandInterface;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.command.SetStatusFinishedCommand;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.command.SetStatusUnavailableCommand;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.command.SetStatusWorkingCommand;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity.Tugas;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository.penugasan.Invoker;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository.penugasan.TugasRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class InvokerTest {
    @Mock
    TugasRepository tugasRepository;

    @Mock
    private Map<String, CommandInterface> commands;

    @InjectMocks
    private Invoker invoker;

    private Invoker invoker2;

    @BeforeEach
    void setup() {
        invoker2 = new Invoker();
    }

    @Test
    void testRegisterCommandMethod() {
        CommandInterface command = new SetStatusFinishedCommand(new Tugas());
        CommandInterface command2 = new SetStatusWorkingCommand(new Tugas());
        CommandInterface command3 = new SetStatusUnavailableCommand(new Tugas());
        invoker2.registerCommand(command);
        invoker2.registerCommand(command2);
        invoker2.registerCommand(command3);
        assertEquals(3, invoker2.getCommands().size());
        assertEquals("java.util.HashMap$KeySet", invoker2.getCommandNames().getClass().getName());
    }

    @Test
    void testExecuteMethodFinishedCommand() {
        CommandInterface command = new SetStatusFinishedCommand(new Tugas());
        when(commands.get("a")).thenReturn(command);
        invoker.execute("a");
        verify(tugasRepository).save(command.getTugas());
    }

    @Test
    void testExecuteMethodWorkingCommand() {
        CommandInterface command2 = new SetStatusWorkingCommand(new Tugas());
        when(commands.get("a")).thenReturn(command2);
        invoker.execute("a");
        verify(tugasRepository).save(command2.getTugas());
    }

    @Test
    void testExecuteMethodUnavailableCommand() {
        CommandInterface command3 = new SetStatusUnavailableCommand(new Tugas());
        when(commands.get("a")).thenReturn(command3);
        invoker.execute("a");
        verify(tugasRepository).save(command3.getTugas());
    }
}

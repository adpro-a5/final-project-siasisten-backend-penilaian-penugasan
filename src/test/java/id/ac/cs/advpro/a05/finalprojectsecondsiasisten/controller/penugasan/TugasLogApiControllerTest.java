package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.controller.penugasan;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.controller.Mapper;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.dto.TugasStatusDTO;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity.TugasLog;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity.TugasStatuses;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penugasan.TugasLogServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest(controllers = TugasLogApiController.class)
class TugasLogApiControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TugasLogServiceImpl tugasLogService;

    private List<TugasLog> tugasListAll;
    private List<TugasLog> tugasList1;
    private TugasStatusDTO tugasStatusDTO;

    @BeforeEach
    void setUp() {
        TugasLog tugasLog = new TugasLog();
        tugasLog.setIdTugasLog(0);
        tugasLog.setEmailAsisten("gmail");
        tugasLog.setIsiLog("log");

        tugasListAll = new ArrayList<>();
        tugasListAll.add(tugasLog);

        tugasList1 = new ArrayList<>();
        tugasList1.add(tugasLog);

        tugasStatusDTO = new TugasStatusDTO();
        tugasStatusDTO.setIdTugas(0);
        tugasStatusDTO.setStatus(TugasStatuses.UNAVAILABLE.name());

    }

    @Test
    void getAllTugasLog() throws Exception {
        when(tugasLogService.fetchAllTugasLog()).thenReturn(tugasListAll);

        mockMvc.perform(get("/api/tugas-log").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].idTugasLog").value(0))
                .andExpect(jsonPath("$[0].emailAsisten").value("gmail"))
                .andExpect(jsonPath("$[0].isiLog").value("log"));
    }

    @Test
    void getAllByIdTugasLog() throws Exception {
        when(tugasLogService.fetchAllByIdTugasLog(0)).thenReturn(tugasList1);

        mockMvc.perform(get("/api/tugas-log/0").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].idTugasLog").value(0))
                .andExpect(jsonPath("$[0].emailAsisten").value("gmail"))
                .andExpect(jsonPath("$[0].isiLog").value("log"));
    }

    @Test
    void getAllByTugasIdTugas() throws Exception {
        when(tugasLogService.fetchAllByTugasIdTugas(0)).thenReturn(tugasList1);

        mockMvc.perform(get("/api/tugas-log/all-log-by-tugas-id/0").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].idTugasLog").value(0))
                .andExpect(jsonPath("$[0].emailAsisten").value("gmail"))
                .andExpect(jsonPath("$[0].isiLog").value("log"));
    }

    @Test
    void postTugasLog() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/tugas-log")
                        .contentType(MediaType.APPLICATION_JSON).content(Mapper.mapToJson(tugasStatusDTO)))
                .andExpect(status().isOk());
    }
}

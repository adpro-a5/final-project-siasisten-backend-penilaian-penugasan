package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.services.penugasan;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity.Tugas;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity.TugasAsistenRelationship;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository.penugasan.TugasAsistenRelationshipRepository;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository.penugasan.TugasRepository;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penugasan.TugasAsistenRelationshipServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TugasAsistenRelationshipServiceTest {
    Class<?> tugasAsistenRelationshipServiceClass;

    @Mock
    TugasAsistenRelationshipRepository tugasAsistenRelationshipRepository;

    @Mock
    TugasRepository tugasRepository;

    @InjectMocks
    private TugasAsistenRelationshipServiceImpl tugasAsistenRelationshipService;

    private TugasAsistenRelationship tugasAsistenRelationship;
    private List<TugasAsistenRelationship> tugasAsistenRelationshipsByTugasId;
    private List<TugasAsistenRelationship> tugasAsistenRelationshipsByEmail;
    private List<TugasAsistenRelationship> tugasAsistenRelationshipsAll;

    @BeforeEach
    void setup() throws Exception {
        tugasAsistenRelationshipServiceClass = Class.forName(
                "id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penugasan.TugasAsistenRelationshipService");

        Tugas tugas = new Tugas();
        tugas.setIdTugas(11);
        Tugas tugas2 = new Tugas();
        tugas2.setIdTugas(22);

        tugasAsistenRelationship = new TugasAsistenRelationship();
        tugasAsistenRelationship.setIdTugasAsistenRelationship(1);
        tugasAsistenRelationship.setEmailAsisten("email");

        TugasAsistenRelationship tugasAsistenRelationship2 = new TugasAsistenRelationship();
        tugasAsistenRelationship2.setIdTugasAsistenRelationship(2);
        tugasAsistenRelationship2.setEmailAsisten("emails");

        tugasAsistenRelationshipsAll = new ArrayList<>();
        tugasAsistenRelationshipsAll.add(tugasAsistenRelationship);
        tugasAsistenRelationshipsAll.add(tugasAsistenRelationship2);

        tugasAsistenRelationshipsByEmail = new ArrayList<>();
        tugasAsistenRelationshipsByEmail.add(tugasAsistenRelationship);

        tugasAsistenRelationshipsByTugasId = new ArrayList<>();
        tugasAsistenRelationshipsByTugasId.add(tugasAsistenRelationship2);
    }

    @Test
    void testFetch() {
        when(tugasAsistenRelationshipRepository.findAllByIdTugasAsistenRelationship(1)).thenReturn(tugasAsistenRelationship);
        assertEquals(tugasAsistenRelationshipService.fetchAllByIdTugasAsistenRelationship(1), tugasAsistenRelationship);

        when(tugasAsistenRelationshipRepository.findAll()).thenReturn(tugasAsistenRelationshipsAll);
        assertEquals(tugasAsistenRelationshipService.fetchAllTugasAsistenRelationship(), tugasAsistenRelationshipsAll);

        when(tugasAsistenRelationshipRepository.findAllByTugasIdTugas(22)).thenReturn(tugasAsistenRelationshipsByTugasId);
        assertEquals(tugasAsistenRelationshipService.fetchAllByTugasIdTugas(22), tugasAsistenRelationshipsByTugasId);

        when(tugasAsistenRelationshipRepository.findAllByEmailAsisten("email")).thenReturn(tugasAsistenRelationshipsByEmail);
        assertEquals(tugasAsistenRelationshipService.fetchAllByEmailAsisten("email"), tugasAsistenRelationshipsByEmail);
    }

    @Test
    void testCreateTugasAsistenRelationship() {
        tugasAsistenRelationshipService.createTugasAsistenRelationship(3, "gmails");
        verify(tugasRepository, times(1)).findAllByIdTugas(3);
    }

    @Test
    void testDeleteTugasAsistenRelationship() {
        tugasAsistenRelationshipService.deleteTugasAsistenRelationship(11, "email");
        verify(tugasAsistenRelationshipRepository, times(1)).findAllByTugasIdTugasAndAndEmailAsisten(11, "email");
    }
}

package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.controller.penugasan;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.controller.Mapper;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.dto.TugasAsistenRelationshipDTO;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity.TugasAsistenRelationship;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penugasan.TugasAsistenRelationshipServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest(controllers = TugasAsistenRelationshipApiController.class)
class TugasAsistenRelationshipApiControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TugasAsistenRelationshipServiceImpl tugasAsistenRelationshipService;

    private List<TugasAsistenRelationship> tugasListAll;
    private List<TugasAsistenRelationship> tugasList1;
    private TugasAsistenRelationshipDTO tugasAsistenRelationshipDTO;
    private TugasAsistenRelationship tugasAsistenRelationship;

    @BeforeEach
    void setUp() {
        tugasAsistenRelationship = new TugasAsistenRelationship();
        tugasAsistenRelationship.setIdTugasAsistenRelationship(0);
        tugasAsistenRelationship.setEmailAsisten("gmail");

        tugasListAll = new ArrayList<>();
        tugasListAll.add(tugasAsistenRelationship);

        tugasList1 = new ArrayList<>();
        tugasList1.add(tugasAsistenRelationship);

        tugasAsistenRelationshipDTO = new TugasAsistenRelationshipDTO();
        tugasAsistenRelationshipDTO.setIdTugas(0);
        tugasAsistenRelationshipDTO.setEmailAsisten("email");

    }

    @Test
    void getAllTugasAsistenRelationship() throws Exception {
        when(tugasAsistenRelationshipService.fetchAllTugasAsistenRelationship()).thenReturn(tugasListAll);

        mockMvc.perform(get("/api/tugas-asisten-relationship").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].idTugasAsistenRelationship").value(0))
                .andExpect(jsonPath("$[0].emailAsisten").value("gmail"));
    }

    @Test
    void getAllByIdTugasAsistenRelationship() throws Exception {
        when(tugasAsistenRelationshipService.fetchAllByIdTugasAsistenRelationship(0)).thenReturn(tugasAsistenRelationship);

        mockMvc.perform(get("/api/tugas-asisten-relationship/0").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.idTugasAsistenRelationship").value(0))
                .andExpect(jsonPath("$.emailAsisten").value("gmail"));
    }

    @Test
    void getAllByTugasIdTugas() throws Exception {
        when(tugasAsistenRelationshipService.fetchAllByTugasIdTugas(0)).thenReturn(tugasList1);

        mockMvc.perform(get("/api/tugas-asisten-relationship/all-relation-by-tugas-id/0")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].idTugasAsistenRelationship").value(0))
                .andExpect(jsonPath("$[0].emailAsisten").value("gmail"));
    }

    @Test
    void getAllByEmail() throws Exception {
        when(tugasAsistenRelationshipService.fetchAllByEmailAsisten("0")).thenReturn(tugasList1);

        mockMvc.perform(get("/api/tugas-asisten-relationship/all-relation-by-email-asisten/0")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].idTugasAsistenRelationship").value(0))
                .andExpect(jsonPath("$[0].emailAsisten").value("gmail"));
    }

    @Test
    void postTugasLog() throws Exception {//
        mockMvc.perform(MockMvcRequestBuilders.post("/api/tugas-asisten-relationship")
                        .contentType(MediaType.APPLICATION_JSON).content(Mapper.mapToJson(tugasAsistenRelationshipDTO)))
                .andExpect(status().isOk());
    }

    @Test
    void deleteTugas() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/tugas-asisten-relationship/delete")
                        .contentType(MediaType.APPLICATION_JSON).content(Mapper.mapToJson(tugasAsistenRelationshipDTO)))
                .andExpect(status().isOk());
    }

}

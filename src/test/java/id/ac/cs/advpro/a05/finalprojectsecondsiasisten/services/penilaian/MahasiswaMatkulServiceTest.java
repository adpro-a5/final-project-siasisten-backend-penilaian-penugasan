package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.services.penilaian;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penilaian.MahasiswaMatkul;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository.penilaian.MahasiswaMatkulRepository;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penilaian.MahasiswaMatkulServiceImpl;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penilaian.PenilaianMahasiswaService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class MahasiswaMatkulServiceTest {
    Class<?> mahasiswaMatkulServiceClass;

    @Mock
    private MahasiswaMatkulRepository mahasiswaMatkulRepository;

    @Mock
    private PenilaianMahasiswaService penilaianMahasiswaService;

    @InjectMocks
    private MahasiswaMatkulServiceImpl mahasiswaMatkulService;

    private List<MahasiswaMatkul> mahasiswaMatkulList;
    private MahasiswaMatkul firstMahasiswa;

    @BeforeEach
    void setup() throws Exception {
        mahasiswaMatkulServiceClass = Class.forName(
                "id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penilaian.MahasiswaMatkulService");
        firstMahasiswa = new MahasiswaMatkul("id", "1");
        MahasiswaMatkul secondMahasiswa = new MahasiswaMatkul("id", "2");
        mahasiswaMatkulList = new ArrayList<>();
        mahasiswaMatkulList.add(firstMahasiswa);
        mahasiswaMatkulList.add(secondMahasiswa);
    }

    @Test
    void testMahasiswaMatkulServiceHasFetchMahasiswaMatkulByIdMethod() throws Exception {
        Method fetchAllMahasiswaMatkul = mahasiswaMatkulServiceClass.getDeclaredMethod(
                "fetchAllMahasiswaMatkul");
        int methodModifiers = fetchAllMahasiswaMatkul.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(0, fetchAllMahasiswaMatkul.getParameterCount());
    }

    @Test
    void testFetchMahasiswaMatkul() {
        when(mahasiswaMatkulRepository.findAll()).thenReturn(mahasiswaMatkulList);
        assertEquals(mahasiswaMatkulService.fetchAllMahasiswaMatkul(), mahasiswaMatkulList);
    }

    @Test
    void testMahasiswaMatkulServiceHasListMahasiswaByIdMethod() throws Exception {
        Method listMahasiswa = mahasiswaMatkulServiceClass.getDeclaredMethod(
                "listMahasiswa",
                String.class);
        int methodModifiers = listMahasiswa.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(1, listMahasiswa.getParameterCount());
    }

    @Test
    void testFetchMahasiswaMatkulById() {
        when(mahasiswaMatkulRepository.findAllByIdMataKuliahOrderByNpmMahasiswaAsc("id")).
                thenReturn(mahasiswaMatkulList);
        assertEquals(mahasiswaMatkulService.listMahasiswa("id"), mahasiswaMatkulList);
    }

    @Test
    void testMahasiswaMatkulServiceHasAddMahasiswaMataKuliahMethod() throws Exception {
        Method addMahasiswaMataKuliah = mahasiswaMatkulServiceClass.getDeclaredMethod(
                "addMahasiswaMataKuliah",
                List.class, String.class);
        int methodModifiers = addMahasiswaMataKuliah.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(2, addMahasiswaMataKuliah.getParameterCount());
    }

    @Test
    void testAddMahasiswaMataKuliah() {
        List<String> mahasiswaList = new ArrayList<>();
        mahasiswaList.add("1");
        mahasiswaList.add("2");
        when(mahasiswaMatkulRepository.findMahasiswaMatkulByIdMataKuliahAndNpmMahasiswa(
                "id", "1")).thenReturn(firstMahasiswa);
        when(mahasiswaMatkulRepository.findMahasiswaMatkulByIdMataKuliahAndNpmMahasiswa(
                "id", "2")).thenReturn(null);
        mahasiswaMatkulService.addMahasiswaMataKuliah(mahasiswaList, "id");
        verify(mahasiswaMatkulRepository, times(1)).save(new MahasiswaMatkul("id", "2"));
        verify(mahasiswaMatkulRepository, times(0)).save(new MahasiswaMatkul("id", "1"));
    }
}

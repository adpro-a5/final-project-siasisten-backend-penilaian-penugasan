package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.services.penilaian;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penilaian.KriteriaPenilaian;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penilaian.PenilaianMahasiswa;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.payload.RequestUpdatePenilaianMahasiswa;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository.penilaian.KriteriaPenilaianRepository;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository.penilaian.PenilaianMahasisiwaRepository;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penilaian.PenilaianMahasiswaServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
class PenilaianMahasiswaServiceTest {
    Class<?> penilaianMahasiswaServiceClass;

    @Mock
    PenilaianMahasisiwaRepository penilaianMahasisiwaRepository;

    @Mock
    KriteriaPenilaianRepository kriteriaPenilaianRepository;

    @InjectMocks
    private PenilaianMahasiswaServiceImpl penilaianMahasiswaServiceImpl;

    private KriteriaPenilaian kriteriaPenilaianLab;
    private List<KriteriaPenilaian> kriteriaPenilaianList;
    private List<PenilaianMahasiswa> penilaianMahasiswaList;
    private RequestUpdatePenilaianMahasiswa mapPenilaian;
    private PenilaianMahasiswa penilaianMahasiswa;

    @BeforeEach
    void setup() throws Exception {
        penilaianMahasiswaServiceClass = Class.forName(
                "id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penilaian.PenilaianMahasiswaService");
        kriteriaPenilaianLab = new KriteriaPenilaian("id", "Lab 1");
        kriteriaPenilaianLab.setId(1);
        KriteriaPenilaian kriteriaPenilaianPR = new KriteriaPenilaian("id", "PR 1");
        kriteriaPenilaianPR.setId(2);
        kriteriaPenilaianList = new ArrayList<>();
        kriteriaPenilaianList.add(kriteriaPenilaianLab);
        kriteriaPenilaianList.add(kriteriaPenilaianPR);
        penilaianMahasiswa = new PenilaianMahasiswa("id", "1", kriteriaPenilaianLab);
        PenilaianMahasiswa penilaianMahasiswa1 = new PenilaianMahasiswa("id", "2", kriteriaPenilaianLab);
        penilaianMahasiswaList = new ArrayList<>();
        penilaianMahasiswaList.add(penilaianMahasiswa);
        penilaianMahasiswaList.add(penilaianMahasiswa1);

        mapPenilaian = new RequestUpdatePenilaianMahasiswa();
        mapPenilaian.setPenilaianMahasiswaList(penilaianMahasiswaList);
    }

    @Test
    void testPenilaianMahasiswaServiceHasFetchPenilaianMahasiswaMethod() throws Exception {
        Method fetchAllPenilaian = penilaianMahasiswaServiceClass.getDeclaredMethod(
                "fetchAllPenilaian");
        int methodModifiers = fetchAllPenilaian.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(0, fetchAllPenilaian.getParameterCount());
    }

    @Test
    void testFetchPenilaianMahasiswa() {
        when(penilaianMahasisiwaRepository.findAll()).thenReturn(penilaianMahasiswaList);
        assertEquals(penilaianMahasiswaServiceImpl.fetchAllPenilaian(), penilaianMahasiswaList);
    }

    @Test
    void testPenilaianMahasiswaServiceHasListPenilaianMethod() throws Exception {
        Method fetchAllPenilaian = penilaianMahasiswaServiceClass.getDeclaredMethod(
                "fetchAllPenilaian");
        int methodModifiers = fetchAllPenilaian.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(0, fetchAllPenilaian.getParameterCount());
    }

    @Test
    void testFetchPenilaianMahasiswaById() {
        when(penilaianMahasisiwaRepository.findAllByIdMataKuliahOrderByNpmMahasiswaAscIdNilaiAsc("id")).thenReturn(penilaianMahasiswaList);
        assertEquals(penilaianMahasiswaServiceImpl.listPenilaian("id"), penilaianMahasiswaList);
    }

    @Test
    void testPenilaianMahasiswaServiceHasAddPenilaianMethod() throws Exception {
        Method addPenilaian = penilaianMahasiswaServiceClass.getDeclaredMethod(
                "addPenilaian",
                List.class, String.class);
        int methodModifiers = addPenilaian.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(2, addPenilaian.getParameterCount());
    }

    @Test
    void testAddPenilaian() {
        List<String> mahasiswaList = new ArrayList<>();
        mahasiswaList.add("1");
        mahasiswaList.add("2");
        when(kriteriaPenilaianRepository.findAllByIdMataKuliah("id")).thenReturn(kriteriaPenilaianList);
        when(penilaianMahasisiwaRepository.findByIdMataKuliahAndNpmMahasiswaAndIdNilai(
                "id", "1", kriteriaPenilaianLab)).thenReturn(penilaianMahasiswa);
        when(penilaianMahasisiwaRepository.findByIdMataKuliahAndNpmMahasiswaAndIdNilai(
                "id", "2", kriteriaPenilaianLab)).thenReturn(null);
        penilaianMahasiswaServiceImpl.addPenilaian(mahasiswaList, "id");
        verify(penilaianMahasisiwaRepository, times(1)).save(new PenilaianMahasiswa("id", "2", kriteriaPenilaianLab));
        verify(penilaianMahasisiwaRepository, times(0)).save(new PenilaianMahasiswa("id", "1", kriteriaPenilaianLab));
    }

    @Test
    void testPenilaianMahasiswaServiceHasUpdateNilaiMethod() throws Exception {
        Method addPenilaian = penilaianMahasiswaServiceClass.getDeclaredMethod(
                "updateNilai",
                RequestUpdatePenilaianMahasiswa.class);
        int methodModifiers = addPenilaian.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(1, addPenilaian.getParameterCount());
    }

    @Test
    void testUpdateNilaiApi() {
        penilaianMahasiswa.setNilai(100);
        penilaianMahasiswaServiceImpl.updateNilai(mapPenilaian);
        verify(penilaianMahasisiwaRepository, times(1)).saveAll(mapPenilaian.getPenilaianMahasiswaList());
    }

}
package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.controller.penugasan;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.controller.Mapper;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.dto.TugasDTO;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.dto.TugasStatusDTO;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity.Tugas;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity.TugasStatuses;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penugasan.InvokerServiceImpl;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penugasan.TugasServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest(controllers = TugasApiController.class)
class TugasApiControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private InvokerServiceImpl invokerService;

    @MockBean
    private TugasServiceImpl tugasService;

    private List<Tugas> tugasListAll;
    private List<Tugas> tugasList1;
    private TugasDTO tugasDTO;
    private TugasStatusDTO tugasStatusDTO;

    @BeforeEach
    void setUp() {
        Tugas tugas1 = new Tugas();
        tugas1.setIdTugas(0);
        tugas1.setIdMataKuliah("0");
        tugas1.setJudulTugas("judul");
        tugas1.setStatusWorking();

        Tugas tugas2 = new Tugas();
        tugas2.setIdTugas(1);
        tugas2.setIdMataKuliah("1");
        tugas2.setJudulTugas("judul2");
        tugas2.setStatusUnavailable();

        Tugas tugas3 = new Tugas();
        tugas3.setIdTugas(2);
        tugas3.setIdMataKuliah("2");
        tugas3.setJudulTugas("judul3");
        tugas3.setStatusFinished();

        tugasListAll = new ArrayList<>();
        tugasListAll.add(tugas1);
        tugasListAll.add(tugas2);
        tugasListAll.add(tugas3);

        tugasList1 = new ArrayList<>();
        tugasList1.add(tugas1);

        tugasDTO = new TugasDTO();
        tugasDTO.setIdMataKuliah("0");
        tugasDTO.setJudulTugas("judul");
        tugasDTO.setDeskripsiTugas("deskripsi");

        tugasStatusDTO = new TugasStatusDTO();
        tugasStatusDTO.setIdTugas(0);
        tugasStatusDTO.setStatus(TugasStatuses.UNAVAILABLE.name());

    }

    @Test
    void getAllTugas() throws Exception {
        when(tugasService.fetchAllTugas()).thenReturn(tugasListAll);

        mockMvc.perform(get("/api/tugas").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[1].idTugas").value(1))
                .andExpect(jsonPath("$[1].idMataKuliah").value("1"))
                .andExpect(jsonPath("$[1].judulTugas").value("judul2"))
                .andExpect(jsonPath("$[1].status").value(TugasStatuses.UNAVAILABLE.name()));
    }

    @Test
    void getAllTugasByIdMataKuliah() throws Exception {
        when(tugasService.fetchAllTugasByMataKuliah("0")).thenReturn(tugasList1);

        mockMvc.perform(get("/api/tugas/0").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].idTugas").value(0))
                .andExpect(jsonPath("$[0].idMataKuliah").value("0"))
                .andExpect(jsonPath("$[0].judulTugas").value("judul"))
                .andExpect(jsonPath("$[0].status").value(TugasStatuses.WORKING.name()));
    }

    @Test
    void postTugas() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/tugas")
                        .contentType(MediaType.APPLICATION_JSON).content(Mapper.mapToJson(tugasDTO)))
                .andExpect(status().isOk());
    }

    @Test
    void deleteTugas() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/tugas/delete/0")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void postTugasStatus() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/tugas/post-status")
                        .contentType(MediaType.APPLICATION_JSON).content(Mapper.mapToJson(tugasStatusDTO)))
                .andExpect(status().isOk());
    }
}

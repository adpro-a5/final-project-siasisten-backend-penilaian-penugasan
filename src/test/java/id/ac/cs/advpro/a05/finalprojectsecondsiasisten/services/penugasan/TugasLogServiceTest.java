package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.services.penugasan;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity.Tugas;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penugasan.entity.TugasLog;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository.penugasan.TugasLogRepository;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository.penugasan.TugasRepository;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penugasan.TugasLogServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TugasLogServiceTest {
    @Mock
    TugasLogRepository tugasLogRepository;

    @Mock
    TugasRepository tugasRepository;

    @InjectMocks
    private TugasLogServiceImpl tugasLogService;

    private List<TugasLog> tugasListAll;
    private List<TugasLog> tugasList1;
    private Tugas tugas;

    @BeforeEach
    void setUp() {
        TugasLog tugasLog = new TugasLog();
        tugasLog.setIdTugasLog(0);
        tugasLog.setEmailAsisten("gmail");
        tugasLog.setIsiLog("log");

        tugasListAll = new ArrayList<>();
        tugasListAll.add(tugasLog);

        tugasList1 = new ArrayList<>();
        tugasList1.add(tugasLog);

        tugas = new Tugas();
    }

    @Test
    void testFetchAllTugasLog() {
        when(tugasLogRepository.findAll()).thenReturn(tugasListAll);
        assertEquals(tugasLogService.fetchAllTugasLog(), tugasListAll);
    }

    @Test
    void testFetchAllByTugasIdTugas() {
        when(tugasLogRepository.findAllByTugasIdTugas(0)).thenReturn(tugasList1);
        assertEquals(tugasLogService.fetchAllByTugasIdTugas(0), tugasList1);
    }

    @Test
    void testFetchAllByIdTugasLog() {
        tugasLogService.createTugasLog(1, "mail", "isi");
        when(tugasLogRepository.findAllByIdTugasLog(0)).thenReturn(tugasList1);
        assertEquals(tugasLogService.fetchAllByIdTugasLog(0), tugasList1);
    }
}
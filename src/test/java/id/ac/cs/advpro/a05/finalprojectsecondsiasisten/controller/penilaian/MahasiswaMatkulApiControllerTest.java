package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.controller.penilaian;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.controller.Mapper;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penilaian.MahasiswaMatkul;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.payload.RequestCreateMahasiswaMatkul;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penilaian.MahasiswaMatkulService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = MahasiswaMatkulApiController.class)
class MahasiswaMatkulApiControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MahasiswaMatkulService mahasiswaMatkulService;

    private List<MahasiswaMatkul> mahasiswaMatkulList;
    private RequestCreateMahasiswaMatkul mapMahasiswa;
    private List<String> stringMahasiswaList;

    @BeforeEach
    void setUp() {
        MahasiswaMatkul firstMahasiswa = new MahasiswaMatkul("id", "1");
        MahasiswaMatkul secondMahasiswa = new MahasiswaMatkul("id", "2");
        mahasiswaMatkulList = new ArrayList<>();
        mahasiswaMatkulList.add(firstMahasiswa);
        mahasiswaMatkulList.add(secondMahasiswa);

        mapMahasiswa = new RequestCreateMahasiswaMatkul();
        stringMahasiswaList = new ArrayList<>();
        stringMahasiswaList.add("1");
        stringMahasiswaList.add("2");
        mapMahasiswa.setIdMataKuliah("id");
        mapMahasiswa.setListMahasiswaMatkul(stringMahasiswaList);
    }

    @Test
    void testGetMahasiswaMatkul() throws Exception {
        when(mahasiswaMatkulService.fetchAllMahasiswaMatkul()).thenReturn(mahasiswaMatkulList);

        mockMvc.perform(get("/api/mahasiswa-matkul").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].idMataKuliah").value("id"))
                .andExpect(jsonPath("$[0].npmMahasiswa").value("1"))
                .andExpect(jsonPath("$[1].npmMahasiswa").value("2"));
    }

    @Test
    void testGetMahasiswaMatkulById() throws Exception {
        when(mahasiswaMatkulService.listMahasiswa("id")).thenReturn(mahasiswaMatkulList);

        mockMvc.perform(get("/api/mahasiswa-matkul/id").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].idMataKuliah").value("id"))
                .andExpect(jsonPath("$[0].npmMahasiswa").value("1"))
                .andExpect(jsonPath("$[1].npmMahasiswa").value("2"));
    }

    @Test
    void testPostMahasiswaMatkul() throws Exception {
        mockMvc.perform(post("/api/mahasiswa-matkul")
                        .contentType(MediaType.APPLICATION_JSON).content(Mapper.mapToJson(mapMahasiswa)))
                .andExpect(status().isOk());

        verify(mahasiswaMatkulService, times(1))
                .addMahasiswaMataKuliah(stringMahasiswaList, "id");
    }
}

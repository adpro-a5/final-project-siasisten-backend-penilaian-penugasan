package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.services.penilaian;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penilaian.KriteriaPenilaian;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penilaian.MahasiswaMatkul;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penilaian.PenilaianMahasiswa;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository.penilaian.KriteriaPenilaianRepository;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository.penilaian.MahasiswaMatkulRepository;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.repository.penilaian.PenilaianMahasisiwaRepository;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penilaian.KriteriaPenilaianServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class KriteriaPenilaianServiceTest {
    Class<?> kriteriaPenilaianServiceClass;

    @Mock
    KriteriaPenilaianRepository kriteriaPenilaianRepository;

    @Mock
    PenilaianMahasisiwaRepository penilaianMahasisiwaRepository;

    @Mock
    MahasiswaMatkulRepository mahasiswaMatkulRepository;

    @InjectMocks
    private KriteriaPenilaianServiceImpl kriteriaPenilaianService;

    private List<KriteriaPenilaian> kriteriaPenilaianList;
    private KriteriaPenilaian lab1;
    private List<MahasiswaMatkul> mahasiswaMatkulList;

    @BeforeEach
    void setup() throws Exception {
        kriteriaPenilaianServiceClass = Class.forName(
                "id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penilaian.KriteriaPenilaianService");
        lab1 = new KriteriaPenilaian("id", "Lab 1");
        KriteriaPenilaian lab2 = new KriteriaPenilaian("id", "Lab 2");
        kriteriaPenilaianList = new ArrayList<>();
        kriteriaPenilaianList.add(lab1);
        kriteriaPenilaianList.add(lab2);

        MahasiswaMatkul firstMahasiswa = new MahasiswaMatkul("id", "1");
        mahasiswaMatkulList = new ArrayList<>();
        mahasiswaMatkulList.add(firstMahasiswa);
    }

    @Test
    void testKriteriaPenilaianServiceHasFetchKriteriaPenilaianMethod() throws Exception {
        Method fetchAllKriteriaPenilaian = kriteriaPenilaianServiceClass.getDeclaredMethod(
                "fetchAllKriteriaPenilaian");
        int methodModifiers = fetchAllKriteriaPenilaian.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(0, fetchAllKriteriaPenilaian.getParameterCount());
    }

    @Test
    void testFetchKriteriaPenilaian() {
        when(kriteriaPenilaianRepository.findAll()).thenReturn(kriteriaPenilaianList);
        assertEquals(kriteriaPenilaianService.fetchAllKriteriaPenilaian(), kriteriaPenilaianList);
    }

    @Test
    void testKriteriaPenilaianServiceHasAddMahasiswaMethod() throws Exception {
        Method fetchAllKriteriaPenilaianByMataKuliah = kriteriaPenilaianServiceClass.getDeclaredMethod(
                "fetchAllKriteriaPenilaianByMataKuliah",
                String.class);
        int methodModifiers = fetchAllKriteriaPenilaianByMataKuliah.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(1, fetchAllKriteriaPenilaianByMataKuliah.getParameterCount());
    }

    @Test
    void testFetchKriteriaPenilaianById() {
        when(kriteriaPenilaianRepository.findAllByIdMataKuliahOrderByNamaNilaiAsc("id")).
                thenReturn(kriteriaPenilaianList);
        assertEquals(kriteriaPenilaianService.fetchAllKriteriaPenilaianByMataKuliah("id"), kriteriaPenilaianList);
    }

    @Test
    void testKriteriaPenilaianServiceHasAddKriteriaPenilaianMethod() throws Exception {
        Method addKriteriaPenilaian = kriteriaPenilaianServiceClass.getDeclaredMethod(
                "addKriteriaPenilaian",
                List.class, String.class);
        int methodModifiers = addKriteriaPenilaian.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(2, addKriteriaPenilaian.getParameterCount());
    }

    @Test
    void testaddKriteriaPenilaian() {
        List<String> kriteriaList = new ArrayList<>();
        kriteriaList.add("Lab 1");
        kriteriaList.add("Lab 2");
        when(mahasiswaMatkulRepository.findAllByIdMataKuliah("id")).thenReturn(mahasiswaMatkulList);
        when(kriteriaPenilaianRepository.findKriteriaPenilaianByNamaNilaiAndIdMataKuliah(
                "Lab 1", "id")).thenReturn(lab1);
        when(kriteriaPenilaianRepository.findKriteriaPenilaianByNamaNilaiAndIdMataKuliah(
                "Lab 2", "id")).thenReturn(null);
        kriteriaPenilaianService.addKriteriaPenilaian(kriteriaList, "id");
        verify(kriteriaPenilaianRepository, times(1)).save(new KriteriaPenilaian("id", "Lab 2"));
        verify(kriteriaPenilaianRepository, times(0)).save(new KriteriaPenilaian("id", "Lab 1"));
        verify(penilaianMahasisiwaRepository, times(1)).save(new PenilaianMahasiswa("id", "1", new KriteriaPenilaian("id", "Lab 2")));
    }
}

package id.ac.cs.advpro.a05.finalprojectsecondsiasisten.controller.penilaian;

import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.controller.Mapper;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.model.penilaian.KriteriaPenilaian;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.payload.RequestCreateKriteriaPenilaian;
import id.ac.cs.advpro.a05.finalprojectsecondsiasisten.service.penilaian.KriteriaPenilaianService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.*;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest(controllers = KriteriaPenilaianApiController.class)
class KriteriaPenilaianApiControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private KriteriaPenilaianService kriteriaPenilaianService;

    private List<KriteriaPenilaian> kriteriaPenilaianList;
    private RequestCreateKriteriaPenilaian mapKriteria;
    private List<String> stringKriteriaList;

    @BeforeEach
    void setUp() {
        KriteriaPenilaian kriteriaPenilaianLab = new KriteriaPenilaian("id", "Lab 1");
        KriteriaPenilaian kriteriaPenilaianPR = new KriteriaPenilaian("id", "PR 1");
        kriteriaPenilaianList = new ArrayList<>();
        kriteriaPenilaianList.add(kriteriaPenilaianLab);
        kriteriaPenilaianList.add(kriteriaPenilaianPR);

        mapKriteria = new RequestCreateKriteriaPenilaian();
        stringKriteriaList = new ArrayList<>();
        stringKriteriaList.add("TP 1");
        stringKriteriaList.add("Lab 2");
        mapKriteria.setIdMataKuliah("id");
        mapKriteria.setListKriteriaPenilaian(stringKriteriaList);
    }

    @Test
    void testGetKriteriaPenilaian() throws Exception {
        when(kriteriaPenilaianService.fetchAllKriteriaPenilaian()).thenReturn(kriteriaPenilaianList);

        mockMvc.perform(get("/api/kriteria-penilaian").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].idMataKuliah").value("id"))
                .andExpect(jsonPath("$[0].namaNilai").value("Lab 1"))
                .andExpect(jsonPath("$[1].namaNilai").value("PR 1"));
    }

    @Test
    void testGetKriteriaPenilaianById() throws Exception {
        when(kriteriaPenilaianService.fetchAllKriteriaPenilaianByMataKuliah("id")).thenReturn(kriteriaPenilaianList);

        mockMvc.perform(get("/api/kriteria-penilaian/id").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].idMataKuliah").value("id"))
                .andExpect(jsonPath("$[0].namaNilai").value("Lab 1"))
                .andExpect(jsonPath("$[1].namaNilai").value("PR 1"));
    }

    @Test
    void testPostKriteriaPenilaian() throws Exception {
        mockMvc.perform(post("/api/kriteria-penilaian")
                        .contentType(MediaType.APPLICATION_JSON).content(Mapper.mapToJson(mapKriteria)))
                .andExpect(status().isOk());

        verify(kriteriaPenilaianService, times(1))
                .addKriteriaPenilaian(stringKriteriaList, "id");
    }
}
